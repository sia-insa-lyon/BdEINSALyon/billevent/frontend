# Billevent frontend app
Partie "utilisateur lambda" de la billetterie *Billevent*. Il se connecte au serveur `billevent-api` via une API HTTP ReST.

## Fonctionnalités manquantes
La partie qui permet de répondre aux questions portant sur des options ne regarde pas la 'Target' de la question, et
par design du serveur, ne peut pas lier une question à un *BilletOption* spécifique (seulement au *Billet*).