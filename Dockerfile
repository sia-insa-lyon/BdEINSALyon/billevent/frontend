FROM node:12-buster AS build

# production ou preprod
ARG environnement=production

WORKDIR /app
COPY package.json package.json
RUN npm install --legacy-peer-dep
COPY *.json /app/
COPY src src
RUN npm run build -- --prod --build-optimizer --configuration=$environnement

FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY nginx/start.sh /start.sh
COPY --from=build /app/dist/* /usr/share/nginx/html/
COPY --from=build /app/src/assets /usr/share/nginx/html/assets

ENV API_URL api.billetterie.bde-insa-lyon.fr
ENV URL_SCHEME https
#AUTOCOMPLETE_PARTICIPANT: true ou false
ENV AUTOCOMPLETE_PARTICIPANT true

CMD /start.sh
