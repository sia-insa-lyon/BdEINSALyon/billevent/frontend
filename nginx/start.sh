#!/bin/sh -e

echo "Configuring UI to use APP Server: $URL_SCHEME://$API_URL"
if [ "${AUTOCOMPLETE_PARTICIPANT}" == "true" ]; then
  echo "le frontend va autocompléter le participant avec les données client"
else
  echo "autocomplétion désactivée"
fi
sed -i -e \
  's#__API_URL__#'"$API_URL"'#g' \
  /usr/share/nginx/html/*.js
sed -i -e \
  's#__URL_SCHEME__#'"$URL_SCHEME"'#g' \
  /usr/share/nginx/html/*.js
sed -i "s/\"__AUTOCOMPLETE_PARTICIPANT__\"/${AUTOCOMPLETE_PARTICIPANT}/g" /usr/share/nginx/html/*.js
echo "removing old compressed files" #en fait y'a pas besoin
find /usr/share/nginx/html/ -name *.gz -exec sh -c 'rm $0' {} \;

echo "compressing files"
gzip -vk9 /usr/share/nginx/html/*

echo "Starting Web Server"
nginx -g 'daemon off;'
