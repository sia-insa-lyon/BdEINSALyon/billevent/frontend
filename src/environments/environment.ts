export const environment = {
  production: false,
  /*apiServer: "http://api-billetterie.172.17.0.1.nip.io",
  jwtDomains: ['api-billetterie.172.17.0.1.nip.io'],*/
  // jwtDomains: ['3da0-134-214-58-112.ngrok.io'],
  jwtDomains: ['localhost:8000'],
  apiServer: "http://localhost:8000",
  cgvUrl: "https://cgv.billetterie.bde-insa-lyon.fr",
  autocompleteParticipant: true
};
