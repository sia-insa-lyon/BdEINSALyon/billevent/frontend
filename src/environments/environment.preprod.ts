export const environment = {
    production: true,
    apiServer: "https://api.billetterie.test.bde-insa-lyon.fr",
    jwtDomains: ['api.billetterie.test.bde-insa-lyon.fr'],
    cgvUrl: "https://cgv.billetterie.bde-insa-lyon.fr"
};
