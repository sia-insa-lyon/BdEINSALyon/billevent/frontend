export const environment = {
  production: true,
  apiServer: "__URL_SCHEME__://__API_URL__",
  jwtDomains: ['__API_URL__'],
  cgvUrl: "https://cgv.billetterie.bde-insa-lyon.fr",
  autocompleteParticipant: "__AUTOCOMPLETE_PARTICIPANT__",
};
