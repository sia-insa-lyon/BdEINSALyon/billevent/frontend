/**
 * Store Coupons retrieved from Server
 * @type {Object<string, Coupon>}
 */
const coupons = {};

export default class Coupon {
    id: number;
    percentage: number;
    amount: number;
    description: string;
    products: number[];
    options: number[];


    constructor(coupon) {
        if (coupons.hasOwnProperty(coupon.id)) {
            return coupons[coupon.id];
        } else {
            this.id = coupon.id;
            this.percentage = parseFloat(coupon.percentage);
            this.amount = parseFloat(coupon.amount);
            this.description = coupon.description;
            this.products = coupon.products;
            this.options = coupon.options;
            coupons[coupon.id] = this;
        }
    }

}