export default class Client {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  numero_VA: string;
  va_validating: boolean;

  constructor(client) {
    this.id = client.id;
    this.first_name = client.first_name;
    this.last_name = client.last_name;
    this.email = client.email;
    this.phone = client.phone;
    this.numero_VA = client.numero_VA;
    this.va_validating = client.va_validating;
  }


  toJson(): Object {
    return {
      id: this.id,
      first_name: this.first_name,
      last_name: this.last_name,
      email: this.email,
      phone: this.phone,
      numero_VA: this.numero_VA
    };
  }
}

export class ClientVA {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  numero_VA: string;

  constructor(client) {
    this.first_name = client.first_name;
    this.last_name = client.last_name;
    this.email = client.email;
    this.phone = client.phone;
    this.numero_VA = client.numero_VA;
  }


  toJson(): Object {
    return {
      first_name: this.first_name,
      last_name: this.last_name,
      email: this.email,
      phone: this.phone,
      numero_VA: this.numero_VA
    };
  }
}

