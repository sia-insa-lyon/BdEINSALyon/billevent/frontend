/**
 * Store PricingRule retrieved from Server
 * @type {Object<string, PricingRule>}
 */
import Order from "./Order";

const rules = {};

class PricingRuleError {
    name: string;
    description: string;
    message: string;

    constructor(name: string, description: string, message: string) {
        this.name = name;
        this.description = description;
        this.message = message;
    }
}

const Errors = {
    BYTI: new PricingRuleError('max', 'Nombre maximum', 'Vous avez atteint le nombre maximal de billets pour votre commande')
}

export default class PricingRule {

    id: number;
    type: string;
    value: number;

    constructor(rule) {
        if (rules.hasOwnProperty(rule.id)) {
            return rules[rule.id];
        } else {
            this.id = rule.id;
            this.type = rule.type;
            this.value = rule.value;
            rules[rule.id] = this;
        }
    }

    checkTotalCount(max: number, order: Order): boolean {
        let count = 0;
        order.billets.forEach((billet) => {
            if (billet.product.rules.filter((rule) => rule.id == this.id).length) {
                count += billet.product.seats || 1;
            }
        });
        console.log(count, max);
        return count <= max;
    }
}
/*
un Map<int,int> superchargé qui fait en sorte que toutes les clés existent
et que toutes les valeurs soient >0
 */
export class RulesMap {
    map: Map<number, number>;

    constructor() {
        this.map = new Map<number, number>();
    }

    get(key: number): number {
        let ret = this.map.get(key);
        if (ret > 0) {
            return ret;
        } else {
            this.map.set(key, 0);
            return 0;
        }
    }

    set(key: number, value: number) {
        this.map.set(key, value);
    }
    change(key,value:number){ //méthode utilisée
        this.set(
            key,
            this.get(key)+value
            );
    }
}