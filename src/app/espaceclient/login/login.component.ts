import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {BilleventApiService} from "../../billevent-api.service";
import {environment} from "../../../environments/environment";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errors: string;
  messages: string;
  username: string;
  password: string;
  reset_pass_url = environment.apiServer+"/accounts/password_reset/";

  constructor(private authservice: BilleventApiService,  private router: Router) { this.router = router}

  ngOnInit() {
  }


  login(){
    this.authservice.login(this.username,this.password).subscribe(
        next => {
          console.log("Etat de l'authentification : "+ (next? "ok":"failed"));
          if(next) {
            this.messages = "Authentification réussie !";
            this.router.navigateByUrl("espaceclient");

          }else {
            this.errors = "Authentification échouée";
          }
        }

    );


  }
}
