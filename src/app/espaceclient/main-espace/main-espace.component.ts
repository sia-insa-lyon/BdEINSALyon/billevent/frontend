import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import Order from "../../../billevent/Order";
import Event from "../../../billevent/Event";
import {EspaceclientApiService} from "../espaceclient-api.service";
import {EspaceclientComponent} from "../espaceclient.component";
import Client from "../../../billevent/Client";
import {Router} from "@angular/router";
import {BilleventApiService} from "../../billevent-api.service";
@Component({
  selector: 'app-main-espace',
  templateUrl: './main-espace.component.html',
  styleUrls: ['./main-espace.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainEspaceComponent implements OnInit {
  orders: Order[];
  events: Event[];
  client: Client;
  printUrl: string;
  show_modif:boolean=false;
  unfinished_order: Order[] = new Array<Order>();

  constructor(private espaceclientservice: EspaceclientApiService, private router: Router, private bas: BilleventApiService) {
      this.printUrl = BilleventApiService.server + "/tickets/";
    this.espaceclientservice.getClient().subscribe(
          next => {
            this.client = next;
            espaceclientservice.getOrders(this.client.id).subscribe(next => this.orders = next);
          },
                  errors => router.navigateByUrl("espaceclient/login")
		  );
	this.bas.listEvents().subscribe(next => this.events=next);
	/*this.espaceclientservice.getMyOrders().subscribe(n => {
	    console.log(n);
	    n.forEach((o)=>{
	        let order=new Order(o);
	        if (order.state<=5){
	            this.unfinished_order.push(order);
            }
        })
	});*/
  }

  ngOnInit() {
  }

  deconnexion() {
      localStorage.removeItem(BilleventApiService.TOKEN_STORAGE_KEY);
      this.router.navigateByUrl("espaceclient/login")
  }
}
