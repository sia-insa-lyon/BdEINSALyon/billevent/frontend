import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {BilleventApiService} from "../../billevent-api.service";
import Client from "../../../billevent/Client";
import {EspaceclientApiService} from "../espaceclient-api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  client= new Client({ "id": null  ,"first_name": null,"last_name": null,"email":null,"phone":null,"numero_VA": null}); //= new Client(JSON.parse("{ 'id': 2  ,'first_name': '','last_name': '','email':'','phone':''}"));
  password: string;
  password2: string;
  errors: string;
  success: boolean;

  constructor(private espaceclientapi: EspaceclientApiService, private authservice: BilleventApiService, private router: Router) {

  }

  submitted = false;

  validate_form(form) {
        if (form.checkValidity() === false) {
            form.classList.add('was-validated');
            return false;
        }
        if(this.password!=this.password2){
            this.errors = "Les mot de passes ne sont pas identiques";
            return false;
        }
        //let regex = new RegExp("(0|\\+33|0033)[1-9][0-9]{8}");

       /* if(regex.exec(this.client.phone)==null){
            this.errors = "Le numéro de téléphone semble être erroné";
            return false;
        }*/

        return true;
    }

  onSubmit() {
      this.errors = "";
      var forms = document.getElementsByClassName('needs-validation');
      for(let i = 0;i<forms.length;i++){
          if(!this.validate_form(forms.item(i))){
              console.log("Non validé !");
              return ;
          }
      }



      this.espaceclientapi.createClient(this.client.last_name,this.client.first_name,this.client.email,this.password,this.client.phone,this.client.numero_VA).subscribe(
          next => {
              this.success = true;
              this.authservice.login(this.client.email,this.password).subscribe(
                  next => {
                      console.log("Etat de l'authentification : "+ (next? "ok":"failed"));
                      if(next) {
                          this.router.navigateByUrl("espaceclient");

                      }else {
                          this.errors = "Authentification échouée";
                      }
                  }

              );
          },
          errors => {
              this.errors = errors.error;
              console.log(errors);
          }
      );
    this.submitted = true;
  }

  ngOnInit() {
  }
  get diagnostic() { return JSON.stringify(this.client); }

}
