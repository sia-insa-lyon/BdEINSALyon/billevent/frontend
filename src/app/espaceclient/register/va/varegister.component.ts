import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {BilleventApiService} from "../../../billevent-api.service";
import Client, {ClientVA} from "../../../../billevent/Client";
import {EspaceclientApiService} from "../../espaceclient-api.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-varegister',
    templateUrl: './varegister.component.html',
    styleUrls: ['./varegister.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VaRegisterComponent implements OnInit {
    client= new Client({ "id": null  ,"first_name": null,"last_name": null,"email":null,"phone":null,"numero_VA": null}); //= new Client(JSON.parse("{ 'id': 2  ,'first_name': '','last_name': '','email':'','phone':''}"));
    password: string;
    password2: string;
    errors: string;
    success: boolean;
    va_validated: boolean;
    clientVa = new ClientVA({ "first_name": null,"last_name": null,"email":null,"phone":null,"numero_VA": null});

    constructor(private espaceclientapi: EspaceclientApiService, private authservice: BilleventApiService, private router: Router) {

    }

    submitted = false;
    step: number;

    validate_form(form) {
        if (form.checkValidity() === false) {
            form.classList.add('was-validated');
            return false;
        }
        return true;
    }

    onSubmit() {
        this.errors = "";
        var forms = document.getElementsByClassName('needs-validation');
        for(let i = 0;i<forms.length;i++){
            if(!this.validate_form(forms.item(i))){
                console.log("Non validé !");
                return ;
            }
        }



        this.espaceclientapi.createClient(this.client.last_name,this.client.first_name,this.client.email,this.password,this.client.phone,this.client.numero_VA).subscribe(
            next => {
                this.success = true;
                if(this.client.numero_VA){
                    this.step = 2;
                }else{
                    this.authservice.login(this.client.email,this.password).subscribe(
                        next => {
                            console.log("Etat de l'authentification : "+ (next? "ok":"failed"));
                            if(next) {
                                this.router.navigateByUrl("espaceclient");

                            }else {
                                this.errors = "Authentification échouée";
                            }
                        }

                    );
                }

            },
            errors => {
                this.errors = errors.error;
                console.log(errors);
            }
        );
        this.submitted = true;
    }

    registerNoVA(){
        this.step = 1;
    }

    onSubmitVA() {
        this.errors = "";
        var forms = document.getElementsByClassName('needs-validation');
        for(let i = 0;i<forms.length;i++){
            if(!this.validate_form(forms.item(i))){
                console.log("Non validé !");
                return ;
            }
        }

        this.espaceclientapi.getInfoFromVA(this.clientVa.numero_VA).subscribe(
            next => {
                this.step = 1;
                this.va_validated = true;
                this.client.first_name = next.first_name;
                this.client.last_name = next.last_name;
                this.client.email = next.email;
                this.client.phone = next.phone;
                this.client.numero_VA = next.numero_VA;
            },
            errors => {
                this.errors = errors.error;
                console.log(errors);
            });

    }

    ngOnInit() {
        this.step = 0;
    }
    get diagnostic() { return JSON.stringify(this.client); }

    loginAndEspace() {
        this.authservice.login(this.client.email,this.password).subscribe(
            next => {
                console.log("Etat de l'authentification : "+ (next? "ok":"failed"));
                if(next) {
                    this.router.navigateByUrl("espaceclient");

                }else {
                    this.errors = "Authentification échouée";
                }
            }

        );
    }
}
