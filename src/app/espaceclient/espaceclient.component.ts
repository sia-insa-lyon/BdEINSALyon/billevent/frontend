import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import Client from "../../billevent/Client";
import {BilleventApiService} from "../billevent-api.service";
import {Router, RouterLink, RouterModule} from "@angular/router";

@Component({
  selector: 'app-espaceclient',
  templateUrl: './espaceclient.component.html',
  styleUrls: ['./espaceclient.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EspaceclientComponent implements OnInit {

  constructor(private billeventapi: BilleventApiService,private router: Router) {

  }

  ngOnInit() {
  }

}
