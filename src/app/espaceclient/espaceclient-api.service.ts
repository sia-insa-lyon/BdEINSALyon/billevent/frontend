import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BilleventApiService} from "../billevent-api.service";
import {Observable} from "rxjs/Observable";
import Order from "../../billevent/Order";
import Client, {ClientVA} from "../../billevent/Client";
import {map} from "rxjs/operators";

@Injectable()
export class EspaceclientApiService {

  constructor(private http: HttpClient) {
  }
  getOrders(clientid:number): Observable<Order[]>{
    return this.http.get<Order[]>(BilleventApiService.server + "/api/client/" + clientid + "/orders/");

  }
    getClient(): Observable<Client>{
        return this.http.get(BilleventApiService.server + "/api/client/").pipe(map(
            (result) => {
                return new Client(result[0])
            }
        ));
    }
    createClient(lastname: string,firstname: string,email: string,password: string,phone: string,va: string): Observable<Client>{
        return this.http.post(BilleventApiService.server + "/api/client/",
            {
                "last_name": lastname,
                "first_name": firstname,
                "email": email,
                "password": password,
                "phone": phone,
                "va": va,
            }
        ).pipe(map(result => {
            return new Client(result)
        }))
    }

    getInfoFromVA(va: string): Observable<ClientVA>{
        return this.http.post(BilleventApiService.server + "/api/client/getVA/",
            {
                "va": va,
            }
        ).pipe(map(result => {
            return new ClientVA(result)
        }))


    }
    /*createUser(login: string, password:string,) {  // non implémenté sur Django
        return this.http.post(BilleventApiService.server + '/api/user/',
            {
                //"email": email,
                "email":login,
                "password": password
            }
        )
    }*/
    getEvents(): Observable<Event[]> {
      return this.http.get<Event[]>(BilleventApiService.TOKEN_STORAGE_KEY + "/api/events/");
    }

    updateClient(client: Client): Observable<Client>{
        return this.http.put(BilleventApiService.server + "/api/client/"+client.id+"/",
            client
        ).pipe(map((result => {
            return new Client(result)
        })))
    }

    getMyOrders(): Observable<Order[]> {
        return this.http.get<Order[]>(BilleventApiService.server+'/api/order/');
    }
}
