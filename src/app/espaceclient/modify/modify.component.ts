import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {EspaceclientApiService} from "../espaceclient-api.service";
import Client from "../../../billevent/Client";
import {Router} from "@angular/router";

@Component({
    selector: 'app-modify',
    templateUrl: './modify.component.html',
    styleUrls: ['./modify.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ModifyComponent implements OnInit {
    @Input() client = new Client({ "id": null  ,"first_name": null,"last_name": null,"email":null,"phone":null,"numero_VA": null});
    @Output() close = new EventEmitter<boolean>();
    success: boolean;
    errors: string;
    step: number;

    constructor(private service: EspaceclientApiService, private router: Router) {
    }

    ngOnInit() {
        this.service.getClient().subscribe(c => this.client = c);
        this.step = 0;
    }

    save() {
        this.service.updateClient(this.client).subscribe(
            (client) => {
                if (client.va_validating) this.step = 1;
                this.client = client;
                this.success = true;
                //this.router.navigateByUrl("espaceclient").then(); //on laisse les gens sur la page pour voir si leur carte VA disparait

            },
            error => this.errors = error
        )
     }

}
