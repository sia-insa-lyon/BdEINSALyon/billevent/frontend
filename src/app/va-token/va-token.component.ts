import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {BilleventApiService} from "../billevent-api.service";
import {ActivatedRoute, Router} from "@angular/router";
import Client from "../../billevent/Client";

@Component({
    selector: 'app-va-token',
    templateUrl: './va-token.component.html',
    styleUrls: ['./va-token.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VaTokenComponent implements OnInit {

    uidb64: string;
    token: string;
    state: string = "loading";
    client: Client;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private billeventApi: BilleventApiService) {
    }

    ngOnInit() {
        this.uidb64 = this.route.snapshot.paramMap.get('uidb64');
        this.token = this.route.snapshot.paramMap.get('token');
        this.billeventApi.validateVa(this.uidb64,this.token).subscribe(
            (client) => {
                this.client = client
                this.state = "success";
                this.router.navigateByUrl('/espaceclient')
            },
            (err) => {
                console.error(err);
                setTimeout(() => {
                    this.state = "failed"
                }, 2500);
            }
        );
    }

}
