import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import Order from "../../../../billevent/Order";
import {BilletOption} from "../../../../billevent/Billet";
import {ShopManagerService} from "../../shop-manager.service";
import {RulesMap} from "../../../../billevent/PricingRule";
import Option from "../../../../billevent/Option";

@Component({
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OptionsComponent implements OnInit {

    @Input()
    order: Order;

    billet_options: Set<BilletOption> = new Set();
    globalOptions = new Set<Option>();
    errors: string;

    // sert à comptabiliser les jauges qui s'étalent sur plusieurs produits (ex: on peut pas prendre plus de 3 vins
    // que ce soit du vin blanc, rouge ou rosé)
    // utilisé pour les options "hôtels"
    rulesCounts: RulesMap = new RulesMap();

    _onRulesChange = false;

    constructor(
        private shopManager: ShopManagerService
    ) {
    }

    ngOnInit() {
        let gbo = [];
        for (let billet of this.order.billets) {
            if(!billet.product) continue;
            for (let option of billet.product.options) {
                if (option.target == 'Order')
                    gbo.push(option);
            }
        }
        gbo.sort((a: Option, b: Option) => {
            if (a.id > b.id)
                return 1;
            if (a.id < b.id)
                return -1;
            return 0
        }).forEach((option) => this.globalOptions.add(option));
        this.order.globalOptions = [];
        window.moveTo(0, 0);
    }

    hasOrderOptions() {
        return this.globalOptions.size > 0;
    }

    updateBilletOption(billet_option: BilletOption) {
        this.billet_options.add(billet_option);
        if(billet_option.billet){
            this.order.billets.forEach((billet) => {
                if (billet.id == billet_option.billet.id) {
                    let NewBilletOptions: BilletOption[] = [];
                    this.billet_options.forEach((bo) => {
                        if (bo.billet.id == billet.id) {
                            NewBilletOptions.push(bo);
                        }
                    });
                    billet.billet_options = NewBilletOptions;
                }
            });
        }else{
            this.order.globalOptions.push(billet_option);
        }
    }

    getTotalPrice() {
        return this.order.getPriceWithCoupon();
    }

    validateOptions() {
        this.shopManager.saveOptions(this.order, this.billet_options).subscribe(
            () => {
                window.scrollTo(0,0);
            },
            (err) => {
                console.error(err);
                this.errors = err.error.error
            }
        )
    }

    onSelectChange(diff: number, boption: Option) {
        console.log("diff", diff);
        for (let i = 0; i < boption.rules.length; i++) {
            console.log("before", this.rulesCounts.get(boption.rules[i].id))
            this.rulesCounts.change(boption.rules[i].id, diff);
            console.log("after", this.rulesCounts.get(boption.rules[i].id));
        }
        this._onRulesChange = !this._onRulesChange; //notifie les enfants
    }
}
