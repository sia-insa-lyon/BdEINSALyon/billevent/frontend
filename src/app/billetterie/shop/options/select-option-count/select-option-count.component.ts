import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {BilletOption, Participant} from "../../../../../billevent/Billet";
import Option from "../../../../../billevent/Option";
import {RulesMap} from "../../../../../billevent/PricingRule";

@Component({
    selector: 'app-select-option-count',
    templateUrl: './select-option-count.component.html',
    styleUrls: ['./select-option-count.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SelectOptionCountComponent implements OnInit {

    @Input()
    participant: Participant;

    @Input()
    option: Option;

    @Output()
    billet_option: EventEmitter<BilletOption> = new EventEmitter();

    @Output()
    onSelectChange: EventEmitter<number> = new EventEmitter<number>();

    _billet_option: BilletOption;

    selected = false;
    available = true;

    @Input()
    rulesMap: RulesMap = new RulesMap();

    @Input()
    set onRulesChanged(oOo: boolean) { //émule un service... cette fonction est appelée par le parent 'OptionsComponent'
        this.option.rules.forEach((rule)=>{
            if(rule.type=='MaxProductByOrder'){
                let count = this.rulesMap.get(rule.id);
                if(count >= rule.value){
                    if (!(this.option.type == 'single' && this.selected))
                        this.available = false;
                    if (this.option.type == 'multiple' && this._billet_option.amount > 0)
                        this.available = true;
                } else {
                    this.available=true;
                }
                console.log(this.option.name,this.available);
            }
        })
    }

    constructor() {
    }

    ngOnInit() {
        this._billet_option = new BilletOption();
        if (this.participant) {
            this._billet_option.billet = this.participant.billet;
            this._billet_option.participant = this.participant;
        }
        this._billet_option.option = this.option;
        this._billet_option.amount = 0;
        this.billet_option.emit(this._billet_option);
        this.possibleCount();
        console.log(this.option)
    }

    updateSingleCount($event: Event) {
        if ((<HTMLInputElement>($event.target)).checked) {
            this._billet_option.amount = 1;
            this.updateRulesCounts(+1);
        } else {
            this.updateRulesCounts(-1);
            this._billet_option.amount = 0;
        }
    }

    updateMultipleCount($event: Event) {
        let amount = parseInt((<HTMLInputElement>($event.target)).value);
        this.updateRulesCounts(amount - this._billet_option.amount);
        this._billet_option.amount = amount;
    }

    selectIfSingle() {
        if (this.option.type === 'single') {
            this.selected = !this.selected;
            this._billet_option.amount = this.selected ? 1 : 0;
        }
    }

    /*
    met à jour la valeur `available` qui conditionne l'affichage "option indisponible"
     */
    possibleCount() {
        /* let max = 12;
         this.option.rules.forEach((rule) => {
             if (rule.type == 'MaxProductByOrder') {
                 max = rule.value
             }
         })
         if (this.option.how_many_left) {
             if (this.option.how_many_left < max && this.option.how_many_left > 0) { //-1 = infini
                 max = this.option.how_many_left;
             }
         }
         if (max <= 0){
             this.available=false;
         }*/
        let max = this.option.how_many_left;
        if (max == -1) {
            max = 12;
        }
        this.option.rules.forEach((rule) => {
            if (rule.type == 'MaxProductByOrder') {
                if (max > rule.value) {
                    max = rule.value
                }
            }
        });
        if (max <= 0) {
            this.available = false;
        }

        let arr = [];
        for (let i = 0; i <= max; i++) {
            arr.push(i)
        }
        return arr;
    }

    updateRulesCounts(diff: number) { //commmunique au parent le changement
        //
        this.onSelectChange.emit(diff);
    }
}
