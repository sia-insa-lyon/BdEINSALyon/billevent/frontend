import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {ShopManagerService} from "../../shop-manager.service";
import Order from "../../../../billevent/Order";
import Billet, {Participant} from "../../../../billevent/Billet";
import Question from "../../../../billevent/Question";
import {NgForm} from "@angular/forms";
import Answer from "../../../../billevent/Answer";
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-questions',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuestionsComponent implements OnInit {

    @Input()
    order: Order;

    ngForm: NgForm;

    participantSaved = false;

    orderQuestions: Set<Question> = new Set();
    answers: Set<Answer> = new Set();

    participantsMap = new Map<number, Participant>(); //map entre l'index dans l'UI et l'ID de participant

    filesMap = new Map<number, File>();

    constructor(private shopManager: ShopManagerService) {
    }

    ngOnInit() {
        this.order.billets.forEach((billet) => {
            billet.product.questions.forEach((question) => {
                if (question.target === "Order") this.orderQuestions.add(question);
            })
        });
        if(this.order.state==2){
            this.participantSaved=false;
        }
        //console.log('event: ',this.order.event); // marche correctement
    }

    participants(billet: Billet) {
        for (let i = billet.participants.length; i < billet.product.seats; i++) {
            billet.participants.push(new Participant(billet));
        }

        return billet.participants;
    }

    updateAnswer(answer: Answer) {
        if (!this.answers.has(answer))
            this.answers.add(answer);
    }

    hasOrderQuestions() {
        return this.orderQuestions.size > 0;
    }

    validateQuestions() {
        console.log(this.ngForm);

    }

    onSubmit(f: NgForm) {
        if (!f.valid) {
            alert("Tous les champs requis n'ont pas été remplis");
            return;
        }

        if (!this.participantSaved)
            this.shopManager.saveParticipants(this.order).subscribe(
                () => {
                    this.participantSaved = true;
                    console.log('_parts', this.participantsMap)
                    this.saveQuestions();
                },
                () => {
                    alert("Les informations des participants ne sont pas valides")
                },
            );
        else
            this.saveQuestions();
        console.log(f.form)
    }

    saveFile(i: number, file: File) {
        this.filesMap.set(i, file)
    }

    private saveQuestions() {
        // enregistrer ici les images
        this.filesMap.forEach((value, key) => {
            this.shopManager.postFile(
                this.filesMap.get(key),
                this.participantsMap.get(key).id
            )
                .subscribe((res) => {
                    console.log(res);
                })
        });

        this.shopManager.saveAnswers(this.order, this.answers).subscribe(
            () => {
            },
            () => {
                alert("Certaines questions n'ont pas de réponse")
            }
        );
    }
}
