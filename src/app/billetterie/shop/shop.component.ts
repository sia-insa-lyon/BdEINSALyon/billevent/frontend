import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import Order from "../../../billevent/Order";
import Event from "../../../billevent/Event";
import {ShopManagerService} from "../shop-manager.service";
import {faCartArrowDown, faCreditCard, faQuestion, faUser, faWineGlass, faExternalLinkAlt} from '@fortawesome/free-solid-svg-icons';
import {environment} from "../../../environments/environment";
import {BilleventApiService} from "../../billevent-api.service";

@Component({
    selector: 'app-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ShopComponent implements OnInit {
    environment = environment;
    faCartsArrowDown = faCartArrowDown
    faUser = faUser
    faWineGlass = faWineGlass
    faQuestion = faQuestion
    faCreditCard = faCreditCard
    faExternalLinkAlt=faExternalLinkAlt

    @Input()
    event: Event;
    order: Order;
    step: number;
    optionsquestions = true;
    loading = true;


    constructor(
        private shopManager: ShopManagerService,
        private billeventApiService: BilleventApiService
    ) {
        this.step = 0;
    }

    ngOnInit() {
        this.loading = true;
        this.shopManager.getCurrentOrder(this.event).subscribe(
            (order) => {
                this.order = order;
                console.log("order ShopCompoennt", order);
                if (!this.order.categories) {//Permet de solve le pb du total gratuit si on recharge la page //TODO: revoir le calcul du TTC
                    this.billeventApiService.getCategories(this.event.id).subscribe(
                        (categories) => {
                            this.order.categories = new Set(categories);
                            this.order.categories.forEach((cat) => {
                                cat.products.forEach((product) => {
                                    this.order.productsCount[product.id] = this.order.countProducts(product);
                                })
                            });
                            this.loading = false;
                        }
                    );
                }else {
                    this.loading = false;
                }
                if (order.state >= 5)//saute les questions d'options si on rafraichit la page au paiement
                    this.optionsquestions = false; //pas ouf mais évite les problèmes
            }, (err) => {
                alert('Erreur');
                console.error(err);
            }
        );
        window.scrollTo(0, 0)
    }

    optionsquestionOk() {
        setTimeout(()=>this.optionsquestions = false,200);
    }
}
