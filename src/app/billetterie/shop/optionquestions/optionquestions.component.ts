import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import Order from "../../../../billevent/Order";
import Question from "../../../../billevent/Question";
import {NgForm} from "@angular/forms";
import {ShopManagerService} from "../../shop-manager.service";
import Answer from "../../../../billevent/Answer";

@Component({
    selector: 'app-optionquestions',
    templateUrl: './optionquestions.component.html',
    styleUrls: ['./optionquestions.component.scss']
})
export class OptionquestionsComponent implements OnInit {

    ngForm: NgForm;

    @Input()
    order: Order;
    answers: Set<Answer> = new Set<Answer>();

    @Output()
    submitted: EventEmitter<boolean> = new EventEmitter<boolean>();

    //<h3>{{boption.billet.product.name}}</h3>

    constructor(private shopManager: ShopManagerService) {
    }

    ngOnInit() {
        console.log("optionquestions: order=", this.order)
        this.order.billets.forEach((billet) => {
            billet.billet_options.forEach((boption: any) => {
                console.log("botion: ", boption);
                boption.option.questions.forEach((question: Question) => {
                    console.log("option question : ", question.question, " help:", question.help_text, boption.billet)
                })
            })
        })
        let bocount = 0;
        for (let billet of this.order.billets) {
            bocount += billet.billet_options.length;
        }
        console.log("bocount", bocount);
        if (bocount == 0) {
            this.submitted.emit(true);
        }

    }

    trackAnswer(answer: Answer) {
        if (!this.answers.has(answer))
            this.answers.add(answer);
    }

    onSubmit(f: NgForm) {
        if (!f.valid) {
            alert("Tous les champs requis n'ont pas été remplis");
            return;
        }
        this.shopManager.saveOptionsAnswers(this.order, this.answers).subscribe(
            () => {
                this.submitted.emit(true);
            },
            () => {
                alert("Certaines questions n'ont pas de réponse")
            }
        );
        console.log(f.form)
    }
}
