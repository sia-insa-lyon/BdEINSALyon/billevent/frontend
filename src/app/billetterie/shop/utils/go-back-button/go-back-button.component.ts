import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import Order from "../../../../../billevent/Order";
import {ShopManagerService} from "../../../shop-manager.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-go-back-button',
    templateUrl: './go-back-button.component.html',
    encapsulation: ViewEncapsulation.None
})
export class GoBackButtonComponent implements OnInit {

    @Input()
    order: Order;

    constructor(
        private shopManagerService: ShopManagerService,
        private router: Router) { }

    ngOnInit() {
    }

    click(){
        this.shopManagerService.goBackOrder(this.order).subscribe(() => {
            window.location.reload(true);
        })
    }

}