import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import Order from "../../../../billevent/Order";
import {ShopManagerService} from "../../shop-manager.service";
import {BilletOption} from "../../../../billevent/Billet";
import {environment} from "../../../../environments/environment";
import {Router} from "@angular/router";

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PaymentComponent implements OnInit {


    @Input()
    order: Order;

    cgvUrl = environment.cgvUrl || "https://cgv.billetterie.bde-insa-lyon.fr";

    constructor(private shopManager: ShopManagerService,
                private router: Router) {
    }

    ngOnInit() {


    }

    pay() {
        this.shopManager.pay(this.order).subscribe(
            (link) => {
                if (link && link.toString().length > 0)
                    window.location.href = link.toString()
                else {
                    this.router.navigate(['billetterie', this.order.event.id, 'payment', this.order.id])
                }
            },
            (err) => {
                if (err.status == 201)
                    this.router.navigate(['billetterie', this.order.event.id, 'payment', this.order.id]);
                else
                    alert("Impossible d'effectuer le paiement");
            }
        )
    }

    getTotalPrice() {
        return this.order.getPriceWithCoupon();
    }

}
