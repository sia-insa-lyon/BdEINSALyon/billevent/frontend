import {Injectable} from '@angular/core';
import Order from "../../billevent/Order";
import Event from "../../billevent/Event";
import {HttpClient} from "@angular/common/http";
import {BilleventApiService} from "../billevent-api.service";
import Billet, {BilletOption, Participant} from "../../billevent/Billet";
import {Observable} from "rxjs/Observable";
import PricingRule from "../../billevent/PricingRule";
import Answer from "../../billevent/Answer";
import {Invitation} from "../../billevent/Invitation";
import {map} from "rxjs/operators";

@Injectable()
export class ShopManagerService {

    constructor(private http: HttpClient) {
    }

    getInvitation(event: Event): Observable<Invitation> {
        return this.http.get(BilleventApiService.server + '/api/events/' + event.id + '/invitation/').pipe(map(
            (invit) => new Invitation(invit)
        ));
    }

    cancelOrder(order: Order) {
        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/cancel/', {})
    }

    goBackOrder(order: Order){
        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/go_back/', {})
    }

    getCurrentOrder(event: Event): Observable<Order> {
        return this.http.get(BilleventApiService.server + '/api/events/' + event.id + '/order/').pipe(
            map((order) => new Order(order)
            ));
    }

    applyCoupon(order: Order, code: string): Observable<Order> {
        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/coupon/', {code}).pipe(map(
            (o) => order.update(o)
        ));
    }

    register(order: Order): Observable<Order> {
        return this.http.post(BilleventApiService.server + '/api/events/' + order.event.id + '/order/', {
            billets: order.billets.map((billet) => {
                return {product: billet.product.id}
            })
        }).pipe(map((o) => {
            order.update(o);
            return order;
        }));
    }

    pay(order: Order) {
        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/pay/', {
            callback: this.callbackFor(order)
        })

    }

    validateRule(order: Order, rule: PricingRule) {

        let billets = order.billets.filter((billet: Billet) => billet.hasRule(rule))

        return new Promise((resolve, reject) => {
            switch (rule.type) {
                case 'MaxSeats':
                    this.http.post(BilleventApiService.server + '/api/rules', {
                        compute: "MaxSeats",
                        data: {
                            products: Array.from(new Set(billets.map((b) => b.product.id))),
                            options: []
                        }
                    }).subscribe((result) => {
                        let count = billets.reduce((value, billet) => value + billet.product.seats, 0);
                        if (result['value'] + count > rule.value) {
                            reject("Jauge maximal atteinte");
                        } else {
                            resolve(true);
                        }
                    }, reject);
                    break;
                case 'MaxProductByOrder':
                    let count = billets.reduce((value, billet) => value + billet.product.seats, 0);
                    if (count > rule.value) {
                        reject("Nombre maximal de place par commande atteinte (" + rule.value + " maximum)");
                    } else {
                        resolve(true);
                    }
                    break;
                case 'CheckMaxProductForInvite':
                    this.http.post(BilleventApiService.server + '/api/rules', {
                        compute: "InvitationsUsed",
                        data: {
                            event: order.event.id
                        }
                    }).subscribe((result) => {
                        let count = billets.reduce((value, billet) => value + billet.product.seats, 0);
                        if (result['value'] + count > result['limit']) {
                            reject("Nombre maximal d'invitation atteinte (" + result['limit'] + " maximum)");
                        } else {
                            resolve(true);
                        }
                    }, reject);
                    break;
                case 'CheckIfVAUsed':
                    this.http.post(BilleventApiService.server + '/api/rules', {
                        compute: "VaUsed",
                        data: {
                            event: order.event.id
                        }
                    }).subscribe(next => {
                        if (next['value'] == "true") {
                            reject("Vous avez déja utilisé votre carte VA pour cet événement !")
                        } else {
                            resolve(true);
                        }
                    }, reject);
                default:
                    resolve(true);
            }
        });
    }

    getFinalOrder(id): Observable<any> {
        return this.http.get(BilleventApiService.server + '/api/order/' + id + '/final/');
    }

    private callbackFor(order: Order) {
        const protocol = window.location.protocol, host = window.location.host;
        return `${protocol}//${host}/billetterie/${order.event.id}/payment/${order.id}`;
    }

    applyCode(order: Order, code: string) {
        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/coupon/', {code}).pipe(map(
            (response: any) => {
                order.update(response);
                return order;
            }
        ))
    }

    saveParticipants(order: Order): Observable<Order> {
        let billets: Map<number, Billet> = new Map();
        let participants: Set<Participant> = new Set();

        order.billets.forEach((billet) => {
            billets.set(billet.id, billet);
            billet.participants.forEach((p) => participants.add(p));
        });

        const data = Array.from(participants).map((p) => p.toJSON());

        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/participants/', data).pipe(map(
            (response: any) => {
                order.state = response.status;
                response.billets.forEach((b) => {
                    billets.get(b.id).participants.forEach((p, i) => {
                        p.id = b.participants[i].id;
                    });
                });

                return order;
            }
        ));
    }

    saveAnswers(order: Order, answers: Set<Answer>): Observable<Order> {
        const data = Array.from(answers).map((p) => p.toJSON());

        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/answers/', data).pipe(map(
            (o: any) => {
                console.log(o);
                order.state = o.status;
                return order;
            }
        ))
    }

    saveOptionsAnswers(order: Order, answers: Set<Answer>): Observable<Order> {
        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/optionsanswers/',
            Array.from(answers).map((p) => p.toJSON())
        ).pipe(map(
            (o: any) => {
                console.log(o);
                order.state = o.status;
                return order;
            }
        ));
    }

    postFile(fileToUpload: File, particiapnt_id: number): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        return this.http.post(BilleventApiService.server + '/api/participant/' + particiapnt_id + '/upload/', formData);
    }

    saveOptions(order: Order, billetOption: Set<BilletOption>): Observable<Order> {
        const data = Array.from(billetOption).filter((bo) => bo.amount > 0).map((p) => p.toJSON());

        return this.http.post(BilleventApiService.server + '/api/order/' + order.id + '/billet_options/', data).pipe(map(
            (o: any) => {
                order.update(o);
                return order;
            }
        ));
    }
}
